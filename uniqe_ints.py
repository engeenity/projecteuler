# https://www.careercup.com/question?id=5750859611766784
def once_ints(ints):
    dt = dict()
    for n in ints:
        dt[n] = dt[n] + 1 if dt.has_key(n) else 1
    return [ k for k in dt.keys() if dt[k]==1]
