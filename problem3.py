# https://projecteuler.net/problem=3

""" Get factorize input integer n into factors less than srt(n) """
def factors(n):
       ub = int(math.sqrt(n))
       return [k for k in range(2, ub) if n % k == 0]

def prob3(n):
    ltemp = factors(n)
    #replace each factor into smaller factors and leave only the number itself if it is prime
    for i in range(len(ltemp)):
        ltemp[i] = factors(ltemp[i]) if len(factors(ltemp[i])) > 0 else [ ltemp[i] ]
    return max([x for sl in ltemp for x in sl])
